const express = require('express');
const redis = require('redis');

const app = express();
const client = redis.createClient({
    host: 'redis-server',
    port: 6379
});

//function dibawah ini bertujuan untuk memastikan bahwa koneksi berhasil
// terhubung kedalam redis sebelum akhirnya dilakukan set nilai counter
client.on('connect', () => {
    console.log('Connected to redis');
    client.set('counter', 0);
});


app.get('/', (req, res) => {
    client.get('counter', (err, counter) => {
        res.send('Page Visit : ' + counter);
        client.set('counter', parseInt(counter) + 1);
    })
});

app.listen(4001, () => {
    console.log('Listening on port 4001')
})